const assert = require("assert");

describe("webdriver.io page", () => {
  before(function() {});

  after(function() {});

  /**
   *    Test for zankoav.prod
   */

  //   it("name1", () => {
  //     browser.url("http://prod.zankoav.com/");
  //     $("#btn").click();
  //     const targettext = $("#text-place").getText();
  //     assert.strictEqual(targettext, "Ready to borsch");
  //   });

  //   it("name2", () => {
  //     browser.url("http://prod.zankoav.com/");
  //     const title = browser.getTitle();
  //     assert.strictEqual(title, "Document");

  /**
   *    Tests for E2E HU desktop
   */

  it("OSF-T7494-Fields validation on Contact Details step", () => {
    browser.url("https://int-e2efleetcor.cs100.force.com/e2eformhu");

    const lastNameValue = $('[id$="lastName"]');
    lastNameValue.setValue("SZALAY");
    console.log("lastNameValue", lastNameValue.getValue());
    assert.strictEqual(lastNameValue.getValue(), "SZALAY");

    const firstNameValue = $('[id$="firstName"]');
    firstNameValue.setValue("Eszter");
    console.log("firstNameValue", firstNameValue.getValue());
    assert.strictEqual(firstNameValue.getValue(), "Eszter");

    const phoneValue = $('[id$="phone"]');
    phoneValue.setValue("532525325");
    console.log("phoneValue", phoneValue.getValue());
    assert.strictEqual(phoneValue.getValue(), "+36532525325");

    const emailValue = $('[id$="email"]');
    emailValue.setValue("test@mail.com");
    console.log("emailValue", emailValue.getValue());
    assert.strictEqual(emailValue.getValue(), "test@mail.com");
  });

  //  function goToTotalConsumptionStep (name){

  //  };

  // it("lastName", () => {
  //   browser.url("https://int-e2efleetcor.cs100.force.com/e2eformhu");
  //   const lastNameValue = $('[id$="lastName"]');
  //   lastNameValue.setValue("SZALAY");
  //   console.log("lastNameValue", lastNameValue.getValue());
  //   assert.strictEqual(lastNameValue.getValue(), "SZALAY");
  // });
  // it("firstName", () => {
  //   browser.url("https://int-e2efleetcor.cs100.force.com/e2eformhu");
  //   const firstNameValue = $('[id$="firstName"]');
  //   firstNameValue.setValue("Eszter");
  //   console.log("firstNameValue", firstNameValue.getValue());
  //   assert.strictEqual(firstNameValue.getValue(), "Eszter");
  // });
  // it("phone", () => {
  //   browser.url("https://int-e2efleetcor.cs100.force.com/e2eformhu");
  //   const phoneValue = $('[id$="phone"]');
  //   phoneValue.setValue("532525325");
  //   console.log("phoneValue", phoneValue.getValue());
  //   assert.strictEqual(phoneValue.getValue(), "+36532525325");
  // });
  // it("email", () => {
  //   browser.url("https://int-e2efleetcor.cs100.force.com/e2eformhu");
  //   const emailValue = $('[id$="email"]');
  //   emailValue.setValue("test@mail.com");
  //   console.log("emailValue", emailValue.getValue());
  //   assert.strictEqual(emailValue.getValue(), "test@mail.com");
  // });

  // it("checkboxPhoneContact", () => {
  //   browser.url("https://int-e2efleetcor.cs100.force.com/e2eformhu");
  //   let result = browser.execute(() => {
  //     // start front
  //     $j('[for="longForm:form:phoneContact"]').click();
  //     let checkbox1 = $j('[id$="phoneContact"]');
  //     // end front
  //     return {
  //       checkbox1: checkbox1.is(":checked")
  //     };
  //   });
  //   assert.equal(result.checkbox1, true);
  //   browser.pause(3000);
  // });

  // it("checkboxinfoByEmail", () => {
  //   browser.url("https://int-e2efleetcor.cs100.force.com/e2eformhu");
  //   let result = browser.execute(() => {
  //     // start front
  //     $j('[for="longForm:form:infoByEmail"]').click();
  //     let checkbox2 = $j('[id$="infoByEmail"]');
  //     // end front
  //     return {
  //       checkbox2: checkbox2.is(":checked")
  //     };
  //   });
  //   assert.equal(result.checkbox2, true);
  //   browser.pause(3000);
  // });
});
