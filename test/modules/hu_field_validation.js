const dataCleaner = require("./../dataCleaner");

describe("Field validation", () => {
  afterEach(async () => {
    let result = await dataCleaner().catch(error =>
      console.log("TCL: error", error)
    );
    console.log("TCL: result", result);
  });

  // require("../test_cases/OSF-T7494");
  // require("../test_cases/OSF-T7495");
  // require("../test_cases/OSF-T7496");
  // require("../test_cases/OSF-T7497");
  require("../test_cases/smokeTest");
});
