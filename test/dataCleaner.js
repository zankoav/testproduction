let request = require("request");
let counter = 0;

function deleteRecords(email) {
  return new Promise((resolve, reject) => {
    const CLIENT_ID =
      "3MVG95AcBeaB55lX.v7LGIE3EZF2YEI525VuZ0HLpDMbvJ96EePN1iMZ_vxqK4iSbESAVlpqb4XikMt5l7NBn";
    const CLIENT_SECRET =
      "A6484329D1BD2885AEB31E37CBCAA58BFAC54F047EFB2CA092E9D4232EE41795";
    const USERNAME = "flt_flash.log@gmail.com.dev027";
    const PASSWORD = "Flt$$123!KU28eImvMkPge6XWMDMH2FqBR";

    let obj;
    let body_1 =
      "grant_type=password" +
      "&client_id=" +
      CLIENT_ID +
      "&client_secret=" +
      CLIENT_SECRET +
      "&username=" +
      USERNAME +
      "&password=" +
      PASSWORD;
    request(
      {
        method: "POST",
        url: "https://test.salesforce.com/services/oauth2/token",
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        body: body_1
      },
      function(error, response, body) {
        console.log("TCL: deleteRecords -> error", error);
        if (error) {
          if (error.port === 443) {
            console.log("GGG error", counter++);
            setTimeout(() => {
              deleteRecords(email);
            }, 1000);
          }
        } else {
          obj = JSON.parse(body);
          if (obj != null) {
            deleteMethod();
          }
        }
      }
    );

    function deleteMethod() {
      request(
        {
          method: "GET",
          url:
            obj.instance_url +
            "/services/data/v46.0/query/?q=SELECT+Id,+Name+,+Account.Id+FROM+Contact+WHERE+Email='" +
            email +
            "'+Limit+1",
          headers: {
            Authorization: "Bearer " + obj.access_token,
            "Content-Type": "application/json"
          }
        },
        function(error, response, body) {
          console.log("TCL: deleteMethod -> error", error);
          let con = JSON.parse(body);
          console.log("con", con);
          if (con.totalSize > 0) {
            main(con.records[0].Account.Id, con.records[0].Id);
          } else {
            reject("deleteMethod error");
          }
        }
      );
    }

    function main(accountId, contactId) {
      request(
        {
          method: "GET",
          url:
            obj.instance_url +
            "/services/data/v46.0/query/?q=SELECT+Id+FROM+Opportunity+WHERE+Account.Id='" +
            accountId +
            "'",
          headers: {
            Authorization: "Bearer " + obj.access_token,
            "Content-Type": "application/json"
          }
        },
        function(error, response, body) {
          console.log("TCL: main -> error", error);
          let arr = [];
          let opp = JSON.parse(body);

          for (let i = 0; i < opp.totalSize; i++) {
            arr.push(opp.records[i].Id);
          }

          if (arr.length != 0) {
            let str = "(";
            for (let i = 0; i < arr.length; i++) {
              str += "'" + arr[i] + "'";
              if (i != arr.length - 1) {
                str += ",";
              }
            }
            str += ")";

            // 1. Health check
            searchRecords(
              obj.instance_url +
                "/services/data/v46.0/query/?q=SELECT+Id+FROM+Health_Check__c+WHERE+Opportunity__c+IN" +
                str
            );
            // 2. Payment Transactions
            searchRecords(
              obj.instance_url +
                "/services/data/v46.0/query/?q=SELECT+Id+FROM+Payment_Transaction__c+WHERE+Opportunity__c+IN" +
                str
            );
            // 3. Credit Factory Report
            searchRecords(
              obj.instance_url +
                "/services/data/v46.0/query/?q=SELECT+Id+FROM+Credit_Factory_Report__c+WHERE+Opportunity__c+IN" +
                str
            );
            // 4. Cards
            searchRecords(
              obj.instance_url +
                "/services/data/v46.0/query/?q=SELECT+Id+FROM+Tankkarten__c+WHERE+Opportunity__c+IN" +
                str
            );
            // 5. Attachments
            searchRecords(
              obj.instance_url +
                "/services/data/v46.0/query/?q=SELECT+Id+FROM+Attachment+WHERE+ParentId+IN" +
                str
            );
            // 6. Opportunity Contact Roles
            searchRecords(
              obj.instance_url +
                "/services/data/v46.0/query/?q=SELECT+Id+FROM+OpportunityContactRoles+WHERE+ContactId=" +
                contactId
            );
            // 7. Opportunity
            searchRecords(
              obj.instance_url +
                "/services/data/v46.0/query/?q=SELECT+Id+FROM+Opportunity+WHERE+AccountId=" +
                accountId
            );
            // 8. Contacts
            searchRecords(
              obj.instance_url +
                "/services/data/v46.0/query/?q=SELECT+Id+FROM+Contact+WHERE+AccountId=" +
                accountId
            );
            // 9. Account
            deleteData(
              obj.instance_url +
                "/services/data/v46.0/sobjects/Account/" +
                accountId
            );
          }
        }
      );
    }

    function searchRecords(myURL) {
      console.log("start myURL", myURL);
      request(
        {
          method: "GET",
          url: myURL,
          headers: {
            Authorization: "Bearer " + obj.access_token,
            "Content-Type": "application/json"
          }
        },
        function(error, response, body) {
          console.log("back myURL", myURL);

          let data;
          try {
            data = JSON.parse(body);
            console.log("data", data);
          } catch (e) {
            console.log("Error JSON", e);
          }

          if (data && data.totalSize > 0) {
            for (let i = 0; i < data.totalSize; i++) {
              deleteData(
                obj.instance_url +
                  "/services/data/v46.0/sobjects/" +
                  data.records[i].attributes.type +
                  "/" +
                  data.records[i].Id
              );
            }
          } else {
            console.log("data searchRecords error", myURL);
          }
        }
      );
    }

    function deleteData(myURL) {
      request(
        {
          method: "DELETE",
          url: myURL,
          headers: {
            Authorization: "Bearer " + obj.access_token,
            "Content-Type": "application/json"
          }
        },
        (error, body) => {
          console.log("TCL: deleteData -> error", error);
          if (error) {
            reject("delete record error");
          } else {
            resolve("Successfully");
          }
        }
      );
    }
  });
}

async function gg() {
  const result = await deleteRecords("dmitry.putyrski@enway.com").catch(error =>
    console.log(error)
  );
  console.log("result", result);
}

// gg();

module.exports = () => {
  return deleteRecords("dmitry.putyrski@enway.com");
};
