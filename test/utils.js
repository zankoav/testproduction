const goToSearchCompanyStep = () => {
  const lastName = $('[id$="lastName"]');
  lastName.setValue("KADAR");

  const firstName = $('[id$="firstName"]');
  firstName.setValue("Csaba");

  const phone = $('[id$="phone"]');
  phone.setValue("532525326");

  const email = $('[id$="email"]');
  email.setValue("dmitry.putyrski@enway.com");

  browser.execute(() => {
    const checkbox1 = $j('[id$="phoneContact"]');
    checkbox1.click();

    const checkbox2 = $j('[id$="infoByEmail"]');
    checkbox2.click();
  });

  const startButton = $(".btn.btn-red.pull-right");
  startButton.click();
};

const goToTotalConsumptionStep = () => {
  goToSearchCompanyStep();

  const companyNameValue = $('[id$="company"]');
  companyNameValue.waitForDisplayed(30000);
  companyNameValue.setValue(
    "Equip-Test Kereskedelmi és Szolgáltató Korlátolt Felelősségű Társaság"
  );

  const cityNameValue = $('[id$="city"]');
  cityNameValue.setValue("Vecsés");

  const buttonSearch = $("a.btn.btn-red.search");
  buttonSearch.click();

  const selectCompany = $(".cf-list");
  selectCompany.waitForDisplayed(20000);
  const companyName = $('[for="12585260"]');
  companyName.click();

  let nextButton = $(".btn.btn-red.continue.save-company-btn");
  nextButton.click();
};

const goToCardConfigurationStep = () => {
  goToTotalConsumptionStep();

  var consumptionValue = $(".totalPumpField");
  consumptionValue.waitForDisplayed(30000);
  consumptionValue.clearValue();
  consumptionValue.setValue("1000");

  var nextButton1 = $(".btn.btn-red.continue");
  nextButton1.click();

  var xxx = $('[id$="creditLimitInput"]');
  xxx.waitForDisplayed(30000);
  let nextButton2 = $(".btn.btn-red.continue");
  nextButton2.click();
};

const goToOfferStep = () => {
  goToCardConfigurationStep();
  let nextButton3 = $(".continue.btnContinueToOffer");
  nextButton3.click();
  var xxx1 = $(".col-lg-3.pull-right");
  xxx1.waitForDisplayed(30000);
};

const goToTechnicalParametersStep = () => {
  goToOfferStep();
  let nextButton4 = $(".continue.btnContinueToTechParams");
  nextButton4.click();
  var xxx2 = $(".b-switch-block");
  xxx2.waitForDisplayed(30000);
};

const goToAdditionalStep = () => {
  goToTechnicalParametersStep();

  var nameOnCardValue = $(".input.ym-disable-keys");
  nameOnCardValue.setValue("test");
  var doneButton = $(".btn.btn-red.btn-done");
  doneButton.click();
  browser.pause(2000);

  let nextButton5 = $(".btn.btn-red.continue");
  nextButton5.click();
  var xxx3 = $(".b-text-img");
  xxx3.waitForDisplayed(30000);
};

const goToDocumentsStep = () => {
  goToAdditionalStep();
  let nextButton6 = $(".btn-red.continue");
  nextButton6.click();
  var xxx4 = $(".b-switch-block");
  console.log("TCL: goToDocumentsStep -> xxx4", xxx4);

  xxx4.waitForDisplayed(30000);
};

const goToPaymentOptionsStep = () => {
  goToDocumentsStep();

  browser.execute(() => {
    const checkbox1 = $j('[id$="checkboxDecision"]');
    checkbox1.click();

    const checkbox2 = $j('[id$="checkboxTermsAndConditions"]');
    checkbox2.click();
  });

  let nextButton7 = $(".btn-red.continue");
  nextButton7.click();
  var xxx4 = $('[id="loading-logo"]');
  xxx4.waitForDisplayed(30000);

  var xxx5 = $('[id="action-bar-btn-continue"]');
  xxx5.waitForDisplayed(30000);
  xxx5.click();

  browser.pause(1000);
  $(".tab-button").click();
  browser.pause(3000);
  // $(".btn.btn-lg.btn-main.item-alt-inline-block").click();
  // var xxx55 = $(".btn.btn-lg.btn-main.item-alt-inline-block");
  // xxx55.waitForDisplayed(30000);
  // xxx55.click();
  browser.pause(3000);
  var xxx6 = $('[id="end-of-document-btn-finish"]');
  xxx6.waitForDisplayed(30000);
  xxx6.click();

  var xxx7 = $(".btn.btn-red");
  xxx7.waitForDisplayed(30000);
  // xxx7.click();

  // let nextButton8 = $(".btn.btn-main.btn-lg");
  // nextButton8.click();
};

module.exports = {
  goToSearchCompanyStep: goToSearchCompanyStep,
  goToTotalConsumptionStep: goToTotalConsumptionStep,
  goToCardConfigurationStep: goToCardConfigurationStep,
  goToOfferStep: goToOfferStep,
  goToTechnicalParametersStep: goToTechnicalParametersStep,
  goToAdditionalStep: goToAdditionalStep,
  goToDocumentsStep: goToDocumentsStep,
  goToPaymentOptionsStep: goToPaymentOptionsStep
};
