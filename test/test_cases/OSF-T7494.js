const assert = require("assert");
const utils = require("./../utils");

it("OSF-T7494 HU Contact details field validation", () => {
  // browser.url("https://int-e2efleetcor.cs100.force.com/e2eformhu");
  browser.url("https://dev027-e2efleetcor.cs102.force.com/e2eformhu");

  const lastName = $('[id$="lastName"]');
  lastName.setValue("SZALAY");
  console.log("=======",lastName.getAttribute("class").includes("success"));

  const firstName = $('[id$="firstName"]');
  firstName .setValue("Eszter");

  const phone = $('[id$="phone"]');
  phone.setValue("532525325");

  const email = $('[id$="email"]');
  email.setValue("dmitry.putyrski@enway.com");
  
  browser.pause(2000);

  assert.strictEqual(lastName.getAttribute("class").includes("success"), true);
  assert.strictEqual(firstName.getAttribute("class").includes("success"), true);
  assert.strictEqual(phone.getAttribute("class").includes("success"), true);
  assert.strictEqual(email.getAttribute("class").includes("success"), true);

});
