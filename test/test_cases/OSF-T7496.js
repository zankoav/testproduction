const assert = require("assert");
const utils = require("./../utils");
const dataCleaner = require("./../dataCleaner");

it("OSF-T7496 HU Total Consumption field validation", () => {
// browser.newWindow("https://int-e2efleetcor.cs100.force.com/e2eformhu");
browser.newWindow("https://dev027-e2efleetcor.cs102.force.com/e2eformhu");

utils.goToTotalConsumptionStep();

var consumptionValue = $('.totalPumpField');
consumptionValue.waitForDisplayed(30000);
consumptionValue.clearValue();
consumptionValue.setValue("1000");
console.log("=======",consumptionValue.getAttribute("class").includes("error"));
browser.pause(2000);

assert.strictEqual(consumptionValue.getAttribute("class").includes("error"), false);

// console.log('start clean');
// dataCleaner();
// browser.pause(10000);
// console.log('end clean');
});