
const assert = require("assert");
const utils = require("./../utils");
const dataCleaner = require("./../dataCleaner");

it("OSF-T7495 HU Search Company field validation", () => {
// browser.newWindow("https://int-e2efleetcor.cs100.force.com/e2eformhu");
browser.url("https://dev027-e2efleetcor.cs102.force.com/e2eformhu");

utils.goToSearchCompanyStep();

const companyNameValue = $('[id$="company"]');
companyNameValue.waitForDisplayed(30000);
companyNameValue.setValue("M-SÉTÁNY Kereskedelmi Szolgáltató és Vendéglátó Korlátolt Felelősségű Társaság")

const cityNameValue = $('[id$="city"]')
cityNameValue.setValue("BUDAPEST");

assert.strictEqual(companyNameValue.getAttribute("class").includes("success"), true);
assert.strictEqual(cityNameValue.getAttribute("class").includes("success"), true);

// console.log('start clean');
// dataCleaner();
// browser.pause(10000);
// console.log('end clean');
});