const assert = require("assert");
const utils = require("./../utils");
const dataCleaner = require("./../dataCleaner");

it("OSF-T7497 HU Card Configuration field validation", () => {
// browser.newWindow("https://int-e2efleetcor.cs100.force.com/e2eformhu");
browser.newWindow("https://dev027-e2efleetcor.cs102.force.com/e2eformhu");

utils.goToCardConfigurationStep();

var numberOfCards = $('[id$="cardsCounter"]');
numberOfCards.waitForDisplayed(30000);

numberOfCards.setValue("0");
console.log("=======",numberOfCards.getValue());
assert.strictEqual(numberOfCards.getValue(), "1");

numberOfCards.setValue("25");
console.log("=======",numberOfCards.getValue());
assert.strictEqual(numberOfCards.getValue(), "25");

numberOfCards.setValue("51");
console.log("=======",numberOfCards.getValue());
assert.strictEqual(numberOfCards.getValue(), "50");

// console.log('start clean');
// dataCleaner();
// browser.pause(10000);
// console.log('end clean');

});